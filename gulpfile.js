/**
 * Created by User on 13.11.2015.
 */
var gulp = require('gulp');
var concat = require ('gulp-concat');
var stylus  = require ('gulp-stylus');


gulp.task('concat', function () {
  return gulp.src('./css/*.css')
    .pipe(concat('all-css.css'))
    .pipe(gulp.dest('./build/'));
});

gulp.task('stylus', function () {
  var config = {
    import: [
      __dirname + '/common/variables.styl'
    ]
  };
  gulp.src('./style/*.styl')
    .pipe(stylus(config))
    .pipe(concat('all-css.css'))
    .pipe(gulp.dest('./build/'));
});

gulp.task('watch', function() {
  gulp.watch('./style/*.styl', ['stylus']);
});


